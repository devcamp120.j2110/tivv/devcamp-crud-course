
const express = require("express");
const mongoose = require("mongoose");
const courseRouter = require("./src/router/CourseRouter");

const port = 3000;
const app = express();
//Khai bao lay tieng viet
app.use(express.urlencoded({
    extended:true
}));
//khai bao body dang JSON
app.use(express.json());
//Connect to mongoDB
async function ConnectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/CRUD_Course");
};
//Excute connect
ConnectMongoDB()
    .then(()=>console.log("Connecto to MongoDB successfully!"))
    .catch(()=>console.log("Connect fail"));

app.get("/", (req, res)=>{
    res.json({
        message: "CRUD cousre API"
    })
});

app.use("/course",courseRouter);

app.listen(port, ()=>{
    console.log(`App listening on port ${port}`);
});
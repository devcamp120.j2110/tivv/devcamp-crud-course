//Import thư viện mongo
const mongoose = require("mongoose");
//Sử dụng phép gán phá hủy cấu trúc để lấy thuộc tính Scheme
const {Schema} = mongoose;
//Khởi tạo Course Scheme MongoDB
const cousreScheme = new Schema(
    {
        _id: Schema.Types.ObjectId,
        title: {
            type: String,
            require: true,
            unique: true
        },
        description: {
            type: String,
            required: false
        },
        noStudent: {
            type: Number,
            default: 0
        }
    }
);
//Tạo course Model
const CourseModel = mongoose.model("Course", cousreScheme);
//Export Course Model
module.exports = {CourseModel};
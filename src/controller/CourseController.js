const mongoose = require("mongoose");
const {CourseModel} = require("../models/CourseModel");

function createCourse (req, res) {
    const course = new CourseModel({
        _id: mongoose.Types.ObjectId(),
        title: req.body.title,
        description: req.body.description,
        noStudent: req.body.noStudent
    });
    course.save()
        .then((newCourse)=>{
            return res.status(200).json({
                message:"Success",
                course: newCourse
            })
        })
        .catch((error)=>{
            return res.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getAllCourse (request, response) {
    CourseModel.find()
        .select("_id title noStudent")
        .then((courseList) => {
            return response.status(200).json({
                message: "Success",
                courses: courseList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

module.exports = {createCourse, getAllCourse};
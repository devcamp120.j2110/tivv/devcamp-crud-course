const express = require("express");
const router = express.Router();
const { createCourse, getAllCourse  } = require("../controller/CourseController");
router.post("/", createCourse);
router.get("/", getAllCourse);

module.exports = router;